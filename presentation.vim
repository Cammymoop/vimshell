
color peachpuff
set cursorline
set number

set shiftwidth=4
set expandtab
set tabstop=4

set ignorecase
set smartcase

" Mappings
let mapleader=","
nnoremap <leader>s :w<cr>
nnoremap Y y$

nnoremap H gT
nnoremap L gt


" " " " " " " "
"   Vimshell  "
" " " " " " " "
nnoremap R :.!bash -v<CR><Esc>

" more complex examples
nnoremap <leader>bb :t.<CR>kI><Esc>j0:.!bash<CR><Esc>
nnoremap <leader>ba o#-#<Esc>k:.!bash -v<CR>o.,/#-#/d<Esc>
match PreProc /^#-#/

" Presentation stuff
" ez run command
nnoremap <leader>x "cyy:<c-r>c<CR>:noh<CR>

" highlighter
function Highlighter()
    call Unhighlighter()
    let w:hlm = matchadd('Error', '\%'.line('.').'l')
endfunction
function Unhighlighter()
    let w:hlm = get(w:, 'hlm', '-')
    if w:hlm != '-'
        call matchdelete(w:hlm)
        let w:hlm = '-'
    endif
endfunction

nnoremap <silent> <leader>h :call Highlighter()<CR>
nnoremap <silent> <leader>dh :call Unhighlighter()<CR>
